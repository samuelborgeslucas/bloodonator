package progweb3.poa.ifrs.edu.aula8;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import progweb3.poa.ifrs.edu.aula8.view.CadastroPacientesActivity;
import progweb3.poa.ifrs.edu.aula8.view.MainActivity;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class GeneralTest {

    @Rule
    public ActivityTestRule<CadastroPacientesActivity> mActivityTestRule = new ActivityTestRule<>(CadastroPacientesActivity.class, false, false);

    @Before
    public void setup(){
        mActivityTestRule.launchActivity(null);
    }

    @After
    public void tearDown(){
    }

    @Test
    public void cadastrarPaciente() {

        Espresso.onView(ViewMatchers.withId(R.id.nomePaciente)).perform(ViewActions.replaceText("Joaquim"));
        Espresso.onView(ViewMatchers.withId(R.id.tipoSanguineo)).perform(ViewActions.replaceText("A+"));
        Espresso.onView(ViewMatchers.withId(R.id.hospital)).perform(ViewActions.replaceText("Conceição"));
        Espresso.onView(ViewMatchers.withId(R.id.qtdDoadores)).perform(ViewActions.replaceText("18"));
        Espresso.onView(ViewMatchers.withId(R.id.cadastrar)).perform(ViewActions.click());

        Espresso.onView(ViewMatchers.withText("Joaquim")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));

    }

    private void resetPreferences(){
        //to do
    }

}
