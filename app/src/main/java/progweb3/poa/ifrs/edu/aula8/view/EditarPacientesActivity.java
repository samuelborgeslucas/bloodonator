package progweb3.poa.ifrs.edu.aula8.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import progweb3.poa.ifrs.edu.aula8.R;
import progweb3.poa.ifrs.edu.aula8.dao.PacienteDAO;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;

public class EditarPacientesActivity extends AppCompatActivity {

    @BindView(R.id.nomePaciente) EditText editTextNomePaciente ;
    @BindView(R.id.tipoSanguineo) EditText editTextTipoSanguineo;
    @BindView(R.id.hospital) EditText editTextHospital;
    @BindView(R.id.qtdDoadores) EditText editTextDoadores;
    @BindView(R.id.editar) Button botaoEditar;

    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_pacientes);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Paciente paciente = (Paciente) getIntent().getParcelableExtra("tarefa");

        id = paciente.getId();
        editTextNomePaciente.setText(paciente.getNomePaciente());
        editTextTipoSanguineo.setText(paciente.getTipoSanguineo());
        editTextHospital.setText(paciente.getHospital());
        editTextDoadores.setText(paciente.getQtdDoadores());

    }

    @OnClick(R.id.editar)
    public void editar(){
        alterar();
    }

    private void alterar(){
        if(editTextNomePaciente.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(), "Nome é obrigatório!", Toast.LENGTH_LONG).show();
            editTextNomePaciente.requestFocus();
        }
        else if(editTextTipoSanguineo.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(), "Descrição é obrigatório!", Toast.LENGTH_LONG).show();
            editTextTipoSanguineo.requestFocus();
        }
        else if(editTextHospital.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(), "Data é obrigatório!", Toast.LENGTH_LONG).show();
            editTextHospital.requestFocus();
        }
        else if(editTextDoadores.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(), "Quantidade de doadores é obrigatória!", Toast.LENGTH_LONG).show();
            editTextDoadores.requestFocus();
        }
        else {
            Paciente paciente = new Paciente();
            paciente.setId(id);
            paciente.setNomePaciente(editTextNomePaciente.getText().toString().trim());
            paciente.setTipoSanguineo(editTextTipoSanguineo.getText().toString().trim());
            paciente.setHospital(editTextHospital.getText().toString().trim());
            paciente.setQtdDoadores(editTextDoadores.getText().toString().trim());

            int linhasAfetadas = new PacienteDAO().update(paciente);
            String msg = "Registro alterado com sucesso! ";
            if(linhasAfetadas == 0 ) msg = "Registro não foi alterado! ";
            //mostrando caixa de diálogo de sucesso
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(R.string.app_name);
            alertDialog.setMessage(msg);
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    // Forçando que o código retorne para a tela de consulta
                    Intent intent = new Intent(getApplicationContext(), ListaPacientesActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                finish();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }

}
