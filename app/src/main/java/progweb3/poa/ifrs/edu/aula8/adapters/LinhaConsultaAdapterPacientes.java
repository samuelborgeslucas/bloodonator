package progweb3.poa.ifrs.edu.aula8.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import progweb3.poa.ifrs.edu.aula8.R;
import progweb3.poa.ifrs.edu.aula8.dao.PacienteDAO;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;
import progweb3.poa.ifrs.edu.aula8.view.DetalhesPacienteActivity;
import progweb3.poa.ifrs.edu.aula8.view.ListaPacientesActivity;

public class LinhaConsultaAdapterPacientes extends BaseAdapter {

    public static String NOME_PACIENTE = "nome_paciente";
    public static String TIPO_SANGUINEO = "tipo_sanguineo";
    public static String HOSPITAL = "hospital";
    public static String QTD_DOADORES = "qtd_doadores";

    private static LayoutInflater layoutInflater = null;

    List<Paciente> pacientes = new ArrayList<>();
    PacienteDAO pacienteDAO;

    private ListaPacientesActivity listarPacientes;

    public LinhaConsultaAdapterPacientes(ListaPacientesActivity listarPacientes, List<Paciente> pacientes){
        this.pacientes = pacientes;
        this.listarPacientes = listarPacientes;
        this.layoutInflater = (LayoutInflater) this.listarPacientes.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pacienteDAO = new PacienteDAO();
    }



    @Override
    public int getCount() {
        return pacientes.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //Cria um objeto para acessar o layout activity_linha.xml
        final View viewLinhaLista = layoutInflater.inflate(R.layout.activity_linha ,null);

        //vincula os campos do arquivo de layout aos objetos cadastrados
        TextView textViewNome           = viewLinhaLista.findViewById(R.id.nomePaciente);
        TextView textViewTipoSanguineo  = viewLinhaLista.findViewById(R.id.tipoSanguineo);
        TextView textViewHospital       = viewLinhaLista.findViewById(R.id.hospital);
        TextView textViewQtdDoadores    = viewLinhaLista.findViewById(R.id.qtdDoadores);
        ImageView imageView = viewLinhaLista.findViewById(R.id.imagem);

        textViewNome.setText(String.valueOf(pacientes.get(position).getNomePaciente()));
        textViewTipoSanguineo.setText(pacientes.get(position).getTipoSanguineo());
        textViewHospital.setText(pacientes.get(position).getHospital());
        textViewQtdDoadores.setText(pacientes.get(position).getQtdDoadores());

//        String txtUrl = "https://goo.gl/7KmcQH";
        String txtUrl = "https://goo.gl/X5dG9J";
        Picasso.get().load(txtUrl).into(imageView);

        Button buttonMaisInformacoes =  viewLinhaLista.findViewById(R.id.botaoMaisInformacoes);

        buttonMaisInformacoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(listarPacientes, DetalhesPacienteActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                intent.putExtra("tarefa", (Parcelable) pacientes.get(position));
                listarPacientes.startActivity(intent);
            }
        });

        return viewLinhaLista;
    }

}
