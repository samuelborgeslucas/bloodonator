package progweb3.poa.ifrs.edu.aula8.app;


import android.app.Application;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BloodonatorApp extends Application {

    private static BloodonatorApp bloodonatorInstance;

    public static BloodonatorApp getInstance(){ return bloodonatorInstance; }

    @Override

    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("pacientes.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded() //deleteRealmIfMigrationNeeded() -> se você mudar a versao do banco com essa linha você apaga todas as informações da versão anterior
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }

    @Override
    public void onTerminate() {
        Realm.getDefaultInstance().close();
        super.onTerminate();
    }
}
