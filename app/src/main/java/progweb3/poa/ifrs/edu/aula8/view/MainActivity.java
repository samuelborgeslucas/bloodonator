package progweb3.poa.ifrs.edu.aula8.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import progweb3.poa.ifrs.edu.aula8.R;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.spinner) Spinner tipoSanguineo;
    @BindView(R.id.botaoPesquisar) Button botaoPesquisar;

//    FirebaseAuth auth;
    private ShareActionProvider mShareActionProvider;
    public static final String TIPO_SANGUINEO = "tipo_sanguineo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar); //vinculando a toolbar ao layout

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        if(LoginActivity.auth.getCurrentUser() == null){
            navigationView.getMenu().findItem(R.id.cadastrarPacientes).setVisible(false);
            navigationView.getMenu().findItem(R.id.listarPacientes).setVisible(false);
            navigationView.getMenu().findItem(R.id.pacientesGson).setVisible(false);
            navigationView.getMenu().findItem(R.id.signout).setVisible(false);
        } else {
            navigationView.getMenu().findItem(R.id.autenticar).setVisible(false);
        }


    }

    @OnClick(R.id.botaoPesquisar)
    public void pesquisar(View view){
        Intent i = new Intent(MainActivity.this, ListaPacientesActivity.class);
        i.putExtra(TIPO_SANGUINEO, tipoSanguineo.getSelectedItem().toString());
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        setMenuActions(id);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setMenuActions(int id) {
        if (id == R.id.cadastrarPacientes) {
            Intent i = new Intent(MainActivity.this, CadastroPacientesActivity.class);
            startActivity(i);
        } else if (id == R.id.listarPacientes) {
            Intent i = new Intent(MainActivity.this, ListaPacientesActivity.class);
            startActivity(i);
        } else if (id == R.id.faq) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ibapcursos.com.br/conheca-tudo-sobre-a-doacao-de-sangue/"));
            startActivity(i);
        } else if (id == R.id.pacientesGson){
            Intent i = new Intent(MainActivity.this, ListaPacientesGsonActivity.class);
            startActivity(i);
        } else if (id == R.id.maps){
            Intent i = new Intent(MainActivity.this, MapsActivity.class);
            startActivity(i);
        } else if (id == R.id.signout) {
            signout();
        } else if (id == R.id.autenticar){
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    private void signout() {
        LoginActivity.auth.signOut();

        if(LoginActivity.auth.getCurrentUser() == null){
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
    }

}
