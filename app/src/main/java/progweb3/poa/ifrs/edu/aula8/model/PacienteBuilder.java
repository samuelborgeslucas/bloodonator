package progweb3.poa.ifrs.edu.aula8.model;

import java.util.ArrayList;
import java.util.List;

public class PacienteBuilder {

    List<PacienteMock> pacienteMocks = new ArrayList<>();

//  String nomePaciente, String tipoSanguineo, String hospital, String qtdDoadores
    PacienteMock joaquim = new PacienteMock("Joaquim", "A+", "Femina", "Precisa de 12 doadores");
    PacienteMock ronaldo = new PacienteMock("Ronaldo", "A-", "Santa Casa", "Precisa de 5 doadores");
    PacienteMock gersony = new PacienteMock("Gersony", "AB+", "Pronto Socorro", "Precisa de 20 doadores");

    public void initPacientes(){
        pacienteMocks.add(joaquim);
        pacienteMocks.add(ronaldo);
        pacienteMocks.add(gersony);
    }

    public List<PacienteMock> getPacienteMocks() {
        return pacienteMocks;
    }
}
