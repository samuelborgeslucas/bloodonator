package progweb3.poa.ifrs.edu.aula8.view;

import android.app.FragmentManager;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import progweb3.poa.ifrs.edu.aula8.R;
import progweb3.poa.ifrs.edu.aula8.dao.PacienteDAO;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;

public class DetalhesPacienteActivity extends AppCompatActivity implements PacienteFragment.OnFragmentInteractionListener {

    @BindView(R.id.nomePaciente)
    TextView txtNomePaciente;
    @BindView(R.id.tipoSanguineo)
    TextView txtTipoSanguineo;
    @BindView(R.id.hospital)
    TextView txtHospital;
    @BindView(R.id.qtdDoadores)
    TextView txtQtdDoadores;
    @BindView(R.id.botaoEditar)
    Button botaoEditar;
    @BindView(R.id.botaoExcluir)
    Button botaoExcluir;
    @BindView(R.id.botaoQueroAjudar)
    Button botaoQueroAjudar;
    @BindView(R.id.imagem) ImageView imagem;


    Paciente pacienteOriginal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_paciente);



        ButterKnife.bind(this);

        if(LoginActivity.auth.getCurrentUser() == null){
            botaoEditar.setVisibility(View.GONE);
            botaoExcluir.setVisibility(View.GONE);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //Pega o objeto que foi passado como parâmetro para essa atividade usando parâmetros intenções
        pacienteOriginal = (Paciente) getIntent().getParcelableExtra("tarefa");

        txtNomePaciente.setText(pacienteOriginal.getNomePaciente());
        txtTipoSanguineo.setText(pacienteOriginal.getTipoSanguineo());
        txtHospital.setText(pacienteOriginal.getHospital());
        txtQtdDoadores.setText(pacienteOriginal.getQtdDoadores());



        String txtUrl = "https://goo.gl/7KmcQH";
        Picasso.get().load(txtUrl).into(imagem);
    }

    @OnClick(R.id.botaoQueroAjudar)
    public void queroAjudar() {
        Intent intent = new Intent(DetalhesPacienteActivity.this, QueroAjudarActivity.class);
        intent.putExtra("tarefa", (Parcelable) pacienteOriginal);
        startActivity(intent);
    }

    @OnClick(R.id.botaoEditar)
    public void editar(){
        Intent intent = new Intent(DetalhesPacienteActivity.this, EditarPacientesActivity.class);
        intent.putExtra("tarefa", (Parcelable) pacienteOriginal);
        startActivity(intent);
    }

    @OnClick(R.id.botaoExcluir)
    public void excluir(){
        PacienteDAO pacienteDAO = new PacienteDAO();
        //Apaga direto no POJO e volta para a lista.
        String mensagem = "Registro excluído com sucesso!";
        //usa o objeto tarefa para fazer a exclusão
        Integer retorno = pacienteDAO.delete(pacienteOriginal);

        if(retorno == -1)
            mensagem = "Erro ao excluir registro!";

        Toast.makeText(DetalhesPacienteActivity.this, mensagem, Toast.LENGTH_LONG).show();
        Intent i = new Intent(DetalhesPacienteActivity.this, ListaPacientesActivity.class);
        startActivity(i);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                finish();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }


}
