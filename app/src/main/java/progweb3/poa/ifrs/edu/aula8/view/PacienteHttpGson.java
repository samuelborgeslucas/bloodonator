package progweb3.poa.ifrs.edu.aula8.view;

import android.util.Log;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;

public class PacienteHttpGson {

    public static final String PACIENTES_URL = "https://gist.githubusercontent.com/samlucax/9d3c9513ca5d6e3ef014e774c9bebd8d/raw/91fd115441e4a38fdcf5522f76a17e3e56929f18/pacientes.json";

    public static List<Paciente> getPacientesFromServer(){
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();

        Request request = new Request.Builder()
                .url(PACIENTES_URL)
                .build();

        try{
            Response response = client.newCall(request).execute();
            String json = response.body().string();
            Log.d("pacientehttp", "JSON = "+json);
            Gson gson = new Gson();
            return Arrays.asList(gson.fromJson(json, Paciente[].class));

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
