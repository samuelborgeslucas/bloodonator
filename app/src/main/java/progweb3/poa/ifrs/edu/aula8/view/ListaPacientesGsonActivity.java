package progweb3.poa.ifrs.edu.aula8.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import progweb3.poa.ifrs.edu.aula8.R;
import progweb3.poa.ifrs.edu.aula8.adapters.LinhaConsultaAdapterPacientesGson;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;

public class ListaPacientesGsonActivity extends AppCompatActivity {

    private FloatingActionButton floatingActionButtonCadastrar;

    private ListView listPacientes;

    private ArrayList<Paciente> pPacientes = new ArrayList<>();
    private PacientesDownloadTask pTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pacientes);

        listPacientes = this.findViewById(R.id.listViewPacientes);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if(pTask == null || pTask.getStatus() != AsyncTask.Status.RUNNING) {
            pTask = new PacientesDownloadTask();
            pTask.execute();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                startActivity(new Intent(this, MainActivity.class));  //O efeito ao ser pressionado do botão (no caso abre a activity)
                finishAffinity();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }

    protected void getAll(){
        List<Paciente> pacientes = PacienteHttpGson.getPacientesFromServer();
        listPacientes.setAdapter(new LinhaConsultaAdapterPacientesGson(this, pacientes));
    }

    class PacientesDownloadTask extends AsyncTask<Void, Void, List<Paciente>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Paciente> doInBackground(Void... strings) {
            return PacienteHttpGson.getPacientesFromServer();
        }

        @Override
        protected void onPostExecute(List<Paciente> pacientes) {
            super.onPostExecute(pacientes);

            if(pacientes != null){
                pPacientes.clear();
                pPacientes.addAll(pacientes);
                listPacientes.setAdapter(new LinhaConsultaAdapterPacientesGson(ListaPacientesGsonActivity.this, pacientes));
            }
        }
    }


}
