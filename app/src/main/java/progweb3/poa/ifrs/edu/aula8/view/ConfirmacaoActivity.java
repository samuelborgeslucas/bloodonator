package progweb3.poa.ifrs.edu.aula8.view;

import android.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import progweb3.poa.ifrs.edu.aula8.R;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;

public class ConfirmacaoActivity extends AppCompatActivity implements PacienteFragment.OnFragmentInteractionListener{

    @BindView(R.id.email) TextView email;
    Paciente paciente;
    String strEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacao);

        ButterKnife.bind(this);

        paciente = getIntent().getParcelableExtra("tarefa");
        strEmail = getIntent().getStringExtra("email");

        email.setText(strEmail);
    }

    @OnClick(R.id.home)
    public void home(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.informacoesPaciente)
    public void visualizarInformacoesPaciente(){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        PacienteFragment pacienteFragment = new PacienteFragment();

        Bundle args = new Bundle();
        args.putString("nomePaciente", paciente.getNomePaciente());
        args.putString("tipoSanguineo", paciente.getTipoSanguineo());
        args.putString("hospital", paciente.getHospital());
        pacienteFragment.setArguments(args);

        fragmentTransaction.replace(R.id.container, pacienteFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
