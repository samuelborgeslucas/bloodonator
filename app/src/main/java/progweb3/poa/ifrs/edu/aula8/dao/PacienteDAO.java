package progweb3.poa.ifrs.edu.aula8.dao;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.Sort;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;

public class PacienteDAO {

    public String insert(String nomePaciente, String tipoSanguineo, String hospital, String qtdDoadores){
        try{
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();

            Paciente paciente = new Paciente();

            try{
                paciente.setId(realm.where(Paciente.class).max("id").intValue() + 1);
            }catch (Exception e){
                paciente.setId(1);
            }

            //setando informacoes do objeto
            paciente.setNomePaciente(nomePaciente);
            paciente.setTipoSanguineo(tipoSanguineo);
            paciente.setHospital(hospital);
            paciente.setQtdDoadores(qtdDoadores);

            realm.insert(paciente);
            realm.commitTransaction();
            return "Registro inserido com sucesso";


        }catch (Exception e){
            e.printStackTrace();
            return "Erro ao inserir registro";
        }
    }

    public Integer delete(final Paciente paciente) {
        try{
            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction(){
                @Override
                public void execute(Realm realm){
                    Paciente p = realm.where(Paciente.class).equalTo("id", paciente.getId()).findFirst();
                    p.deleteFromRealm();
                }
            });
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return -1;
        }
    }

    public int update(Paciente paciente) {
        try{
            Realm realm = Realm.getDefaultInstance();
            Paciente p = realm.where(Paciente.class).equalTo("id", paciente.getId()).findFirst();

            realm.beginTransaction();

            p.setNomePaciente(paciente.getNomePaciente());
            p.setTipoSanguineo(paciente.getTipoSanguineo());
            p.setHospital(paciente.getHospital());
            p.setQtdDoadores(paciente.getQtdDoadores());

            realm.commitTransaction();
            return 1;
        }catch (Exception e){
            e.printStackTrace();
        }

        return -1;
    }

    public List<Paciente> getAll(){

        List<Paciente> pacientes = new ArrayList<>();

        try{
            Realm realm = Realm.getDefaultInstance();
            pacientes.addAll(realm.where(Paciente.class).findAll());
            return pacientes;
        }catch (Exception e){
            e.printStackTrace();
        }

        return pacientes;
    }

    public List<Paciente> getAllWithBloodType(String bloodType){

        List<Paciente> pacientes = new ArrayList<>();

        try{
            Realm realm = Realm.getDefaultInstance();
            pacientes.addAll(realm.where(Paciente.class)
                    .equalTo("tipoSanguineo", bloodType)
                    .sort("qtdDoadores", Sort.DESCENDING)
                    .findAll());
            return pacientes;
        }catch (Exception e){
            e.printStackTrace();
        }

        return pacientes;

    }

}
