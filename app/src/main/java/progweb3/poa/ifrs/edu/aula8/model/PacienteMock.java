package progweb3.poa.ifrs.edu.aula8.model;

import java.util.Random;

public class PacienteMock {

    public static final String[] nomes = { "Joaquim", "Mauro", "Mary" };
    public static final String[] tiposSanguineos = { "A+", "A-", "B+", "B-", "AB+"};
    public static final String[] hospitais = { "Hospital Santa Casa", "Hospital Femina", "Hospital Pronto Socorro"};

    private String nomePaciente;
    private String tipoSanguineo;
    private String hospital;
    private String qtdDoadores;

    public PacienteMock(){}

    public PacienteMock(String nomePaciente, String tipoSanguineo, String hospital, String qtdDoadores) {
        this.nomePaciente = nomePaciente;
        this.tipoSanguineo = tipoSanguineo;
        this.hospital = hospital;
        this.qtdDoadores = qtdDoadores;
    }

    public String getNomePaciente() {
        return nomePaciente;
    }

    public void setNomePaciente(String nomePaciente) {
        this.nomePaciente = nomePaciente;
    }

    public String getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(String tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getQtdDoadores() {
        return qtdDoadores;
    }

    public void setQtdDoadores(String qtdDoadores) {
        this.qtdDoadores = qtdDoadores;
    }

    public static PacienteMock carrega() {
        return new PacienteMock(
                nomes[getRandomValue(0, 5)],
                tiposSanguineos[getRandomValue(0, 5)],
                hospitais[getRandomValue(0, 5)],
                String.valueOf(getRandomValue(0, 15)));
    }

    private static int getRandomValue(int low, int high) {
        return new Random().nextInt(high - low) + low;
    }

    @Override
    public String toString() {
        return "PacienteMock{" +
                "nomePaciente='" + nomePaciente + '\'' +
                ", tipoSanguineo='" + tipoSanguineo + '\'' +
                ", hospital='" + hospital + '\'' +
                ", qtdDoadores='" + qtdDoadores + '\'' +
                '}';
    }
}
