package progweb3.poa.ifrs.edu.aula8.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import progweb3.poa.ifrs.edu.aula8.R;
import progweb3.poa.ifrs.edu.aula8.dao.PacienteDAO;

public class CadastroPacientesActivity extends AppCompatActivity {

    @BindView(R.id.nomePaciente) EditText nomePaciente;
    @BindView(R.id.tipoSanguineo) EditText tipoSanguineo;
    @BindView(R.id.hospital) EditText hospital;
    @BindView(R.id.qtdDoadores) EditText qtdDoadores;

    @BindView(R.id.cadastrar) Button botao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_pacientes);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ButterKnife.bind(this);

        limparCampos();
    }

    @OnClick(R.id.cadastrar)
    public void cadastrar(){
        PacienteDAO pacienteDAO = new PacienteDAO();

        String resultado = pacienteDAO.insert(nomePaciente.getText().toString(), tipoSanguineo.getText().toString(),
                hospital.getText().toString(), qtdDoadores.getText().toString());

        Toast.makeText(getApplicationContext(), resultado, Toast.LENGTH_LONG).show();

        limparCampos();

        Intent i = new Intent(CadastroPacientesActivity.this, ListaPacientesActivity.class);
        startActivity(i);
    }

    private void limparCampos(){
        nomePaciente.setText("");
        tipoSanguineo.setText("");
        hospital.setText("");
        qtdDoadores.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                startActivity(new Intent(this, MainActivity.class));
                finishAffinity();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }
}
