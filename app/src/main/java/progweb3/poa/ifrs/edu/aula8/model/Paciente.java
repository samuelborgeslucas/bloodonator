package progweb3.poa.ifrs.edu.aula8.model;

/*
* Annotations usadas no Realm
@Required - diz ao Realm que o campo é necessário.
@Ignore  - informa que o campo não será persistido no arquivo/disco
@Index - adiciona um índice ao campo. Isso faz com que as consultas fiquem mais rápidas
@PrimaryKey - define que o campo é indexado
Para mais detalhes sobre as anotações e mapeamentos veja o exemplo:
https://github.com/realm/realm-java/blob/master/examples/introExample/src/main/java/io/realm/examples/intro/model/Person.java
*/


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Paciente extends RealmObject implements Serializable, Parcelable {

    @PrimaryKey
    private int id;
    private String nomePaciente;
    private String tipoSanguineo;
    private String hospital;
    private String qtdDoadores;

    public Paciente(){}
    public Paciente(int id, String nomePaciente, String tipoSanguineo, String hospital, String qtdDoadores){
        this.id = id;
        this.nomePaciente = nomePaciente;
        this.tipoSanguineo = tipoSanguineo;
        this.hospital = hospital;
        this.qtdDoadores = qtdDoadores;
    }

    private Paciente(Parcel from){
        id = from.readInt();
        nomePaciente = from.readString();
        tipoSanguineo = from.readString();
        hospital = from.readString();
        qtdDoadores = from.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomePaciente() {
        return nomePaciente;
    }

    public void setNomePaciente(String nomePaciente) {
        this.nomePaciente = nomePaciente;
    }

    public String getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(String tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getQtdDoadores() {
        return qtdDoadores;
    }

    public void setQtdDoadores(String qtdDoadores) {
        this.qtdDoadores = qtdDoadores;
    }

    //importante: cada classe do projeto deve ter um identificador diferente.
    @Override
    public int describeContents() { return 1; }

    @Override
    public void writeToParcel(Parcel dest, int i){
        dest.writeInt(id);
        dest.writeString(nomePaciente);
        dest.writeString(tipoSanguineo);
        dest.writeString(hospital);
        dest.writeString(qtdDoadores);
    }

    public static final Parcelable.Creator<Paciente>
        CREATOR = new Parcelable.Creator<Paciente>(){
            public Paciente createFromParcel(Parcel in) {return new Paciente(in); }
            public Paciente[] newArray(int size) { return new Paciente[size]; }
        };

}