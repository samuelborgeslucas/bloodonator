package progweb3.poa.ifrs.edu.aula8.view;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.List;

import progweb3.poa.ifrs.edu.aula8.R;
import progweb3.poa.ifrs.edu.aula8.adapters.LinhaConsultaAdapterPacientes;
import progweb3.poa.ifrs.edu.aula8.dao.PacienteDAO;
import progweb3.poa.ifrs.edu.aula8.model.Paciente;

public class ListaPacientesActivity extends AppCompatActivity {

    private ListView listPacientes;
    private String bloodType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pacientes);

        listPacientes = this.findViewById(R.id.listViewPacientes);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        bloodType = getIntent().getStringExtra("tipo_sanguineo");
        this.getResults(bloodType);

    }

    protected void getResults(String parameter){
        if(parameter!= null && !parameter.isEmpty()){
            getAllWithBloodType(parameter);
        }else{
            getAll();
        }
    }

    protected void getAll(){
        PacienteDAO pacienteDAO = new PacienteDAO();
        List<Paciente> pacientes = pacienteDAO.getAll();
        listPacientes.setAdapter(new LinhaConsultaAdapterPacientes(this, pacientes));
    }

    protected void getAllWithBloodType(String bloodType){
        PacienteDAO pacienteDAO = new PacienteDAO();
        List<Paciente> pacientes = pacienteDAO.getAllWithBloodType(bloodType);
        listPacientes.setAdapter(new LinhaConsultaAdapterPacientes(this, pacientes));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                startActivity(new Intent(this, MainActivity.class));  //O efeito ao ser pressionado do botão (no caso abre a activity)
                finishAffinity();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }


}
