package progweb3.poa.ifrs.edu.aula8.view;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import progweb3.poa.ifrs.edu.aula8.R;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        LatLng santaCasa = new LatLng(-30.0303677, -51.2237683);
        LatLng hospDeClinicas = new LatLng(-30.0387223, -51.2073265);
        LatLng hemocentroRs = new LatLng(-30.0627793, -51.1806828);
        LatLng labMarquesPereira = new LatLng(-30.0313296, -51.2269425);
        LatLng hospSaoLucasPucrs = new LatLng(-30.0551241, -51.1772088);
        LatLng hospMaeDeDeus = new LatLng(-30.0586531, -51.2313639);
        LatLng hospConceicao = new LatLng(-30.0159434, -51.1606252);
        LatLng hospDivinaProvidencia = new LatLng(-30.0848175, -51.1901265);
        LatLng hospMoinhosDeVento = new LatLng(-30.0256917, -51.2105876);

        mMap.addMarker(new MarkerOptions().position(santaCasa).title("Santa Casa"));
        mMap.addMarker(new MarkerOptions().position(hospDeClinicas).title("Hosp de Clínicas"));
        mMap.addMarker(new MarkerOptions().position(hemocentroRs).title("Hemocentro"));
        mMap.addMarker(new MarkerOptions().position(labMarquesPereira).title("Lab Marques Pereira"));
        mMap.addMarker(new MarkerOptions().position(hospSaoLucasPucrs).title("Hosp São Lucas PUCRS"));
        mMap.addMarker(new MarkerOptions().position(hospMaeDeDeus).title("Hosp Mãe de Deus"));
        mMap.addMarker(new MarkerOptions().position(hospConceicao).title("Hosp Conceição"));
        mMap.addMarker(new MarkerOptions().position(hospDivinaProvidencia).title("Hosp Divina Providencia"));
        mMap.addMarker(new MarkerOptions().position(hospMoinhosDeVento).title("Hosp Moinhos de Vento"));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hospDeClinicas, 13));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Botão adicional na ToolBar
        switch (item.getItemId()) {
            case android.R.id.home:  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
                startActivity(new Intent(this, MainActivity.class));  //O efeito ao ser pressionado do botão (no caso abre a activity)
                finishAffinity();  //Método para matar a activity e não deixa-lá indexada na pilhagem
                break;
            default:break;
        }
        return true;
    }

}
